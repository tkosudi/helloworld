import { Component } from "react";

class HelloWorld extends Component {
  render() {
    return <div>Olá mundo!</div>;
  }
}

export default HelloWorld;
